import $ from 'jquery';
import 'bootstrap';
import{API_KEY} from './constants';




var script = document.createElement('script');
script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAg9H_P1AFYNAmEJlTj5-X9M-w1gpWsyFA&callback=initMap';
script.defer = true;
script.async = true;

// Attach your callback function to the `window` object
window.initMap = function() {
  // JS API is loaded and available
  var monash = { lat: -37.8771, lng: 145.0449}
};
var map;
function initMap() {
  map = new google.maps.Map(document.getElementById("map"), {
    center: monash,
    zoom: 16,
    styles: [{
    "featureType": "road.arterial",
    "elementType": "geometry",
    "stylers": [
      { "color": "#EBEBEB" }
    ]
  }]
  });

  var contentString = '<div id="content">'+
     '<div id="siteNotice">'+
     '</div>'+
     '<h1 id="firstHeading" class="firstHeading">Monash Uni</h1>'+
     '<div id="bodyContent">'+
     '<p>' +
     'One of the top 5 universities in Australia'+
     '</p>'+
     '</div>'+
     '</div>';


  var infowindow = new google.maps.InfoWindow({
      content: contentString
    });

  var marker = new google.maps.Marker({
    position: monash,
    map: map,
    animation: google.maps.Animation.DROP,
    title: 'Monash Uin is where we can meet'

  });

  marker.addListener('click', function() {
   infowindow.open(map, marker);
 });
}
// Append the 'script' element to 'head'
document.head.appendChild(script);

window.$ = $;
window.jQuery = $;
